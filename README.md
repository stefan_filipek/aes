# Key derivation examples

For AES-256:
```
python aes.py --round_key de1369676ccc5a71fa2563959674ee155886ca5d2e2f31d77e0af1fa27cf73c3 --round_num 5
Derived key: '603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4'
```


For AES-128:
```
python aes.py --round_key ac7766f319fadc2128d12941575c006e --round_num 9
Derived key: '2b7e151628aed2a6abf7158809cf4f3c'
```